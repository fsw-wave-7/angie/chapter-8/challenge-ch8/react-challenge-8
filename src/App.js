// import { Container, Row } from 'react-bootstrap';

import Header from './components/header'
import Player from './pages/player'

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { Fragment } from 'react';


function App() {
  return (
    <Fragment>
      <Header />
      <Player />
    </Fragment>
  );
}

export default App;
