import React, { Component, Fragment } from 'react'
import { Col, Row, Form, Button, Table, Container } from 'react-bootstrap'

class Player extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id: null,
            keyword: "",
            username: "",
            email: "",
            password: "",
            exp: "",
            lvl: "",
            data: [
                {
                    id: "1",
                    username: "Angie",
                    email: "angie@gmail.com",
                    password: "1234",
                    exp: "1000",
                    lvl: "1",
                },
                {
                    id: "2",
                    username: "robby",
                    email: "robby@gmail.com",
                    password: "1234",
                    exp: "2000",
                    lvl: "2",
                }
            ]
        }
        this.handleChange = this.handleChange.bind(this)
        this.submitData = this.submitData.bind(this);
        this.edit = this.edit.bind(this);
        this.find = this.find.bind(this)
    }

    handleChange(key, value) {
        this.setState({
            [key]: value
        })
    }
    submitData(event) {
        event.preventDefault()
        const { data, username, email, password, exp, lvl, id } = this.state
        if (id) {
            const { data } = this.state
            const playerYangTidakDipilih = data.filter((player) => { return player.id !== id })
            playerYangTidakDipilih.push({ username, email, password, exp, lvl, id })
            this.setState({ data: playerYangTidakDipilih })

        } else {
            data.push({ username, email, password, exp, lvl, id: data.length + 1 })
            this.setState({ data: data })
        }
    }
    edit = (index) => {
        const { data } = this.state
        const newData = data.filter((player) => { return player.id === index })


        this.setState({
            id: newData[0].id,
            username: newData[0].username,
            email: newData[0].email,
            password: newData[0].password,
            exp: newData[0].exp,
            lvl: newData[0].lvl,
        })
    }
    find() {
        const { data, keyword } = this.state;
        const search = data.filter((hasilFilter) => {
            return (
                hasilFilter.username.toLowerCase().includes(keyword.toLowerCase()),
                hasilFilter.email.toLowerCase().includes(keyword.toLowerCase()),
                hasilFilter.exp.includes(keyword),
                hasilFilter.lvl.includes(keyword)
            )
        })
        console.log(search)

        this.setState({
            data: search
        })
    }

    render() {
        const { data, username, email, password, exp, lvl, keyword } = this.state
        return (
            <Fragment>
                <Container className="p-2 m-5">
                    <Row>
                        <Col>
                            <Form>
                                <Row className="mb-5">
                                    <Col className="col-md-10">
                                        <Form.Control
                                            placeholder="Search"
                                            value={keyword}

                                            onChange={(event) => {
                                                this.handleChange('keyword', event.target.value)
                                            }} />
                                    </Col>
                                    <Col className="col-md-2">
                                        <Button variant="primary" onClick={this.find}>Search</Button>
                                    </Col>
                                </Row>
                            </Form>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Password</th>
                                        <th>Experience</th>
                                        <th>Level</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody >
                                    {
                                        data.map((item, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{index + 1}</td>
                                                    <td>{item.username}</td>
                                                    <td>{item.email}</td>
                                                    <td>{item.password}</td>
                                                    <td>{item.exp}</td>
                                                    <td>{item.lvl}</td>
                                                    <td> <Button variant="primary" onClick={() => this.edit(item.id)}>Edit</Button></td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </Table>
                        </Col>
                        <Col>
                            <Form onSubmit={this.submitData}>
                                <Form.Group className="mb-3" controlId="username">
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Input your username"
                                        value={username}
                                        onChange={(event) => this.handleChange('username', event.target.value)}
                                    />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="email">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Input your Email address"
                                        value={email}
                                        onChange={(event) => this.handleChange('email', event.target.value)}
                                    />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="password">
                                    <Form.Label> Password </Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Input your password"
                                        value={password}
                                        onChange={(event) => this.handleChange('password', event.target.value)}
                                    />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="exp">
                                    <Form.Label> Experience </Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Input your experience"
                                        value={exp}
                                        onChange={(event) => this.handleChange('exp', event.target.value)}
                                    />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="lvl">
                                    <Form.Label> Level </Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Input your lvl"
                                        value={lvl}
                                        onChange={(event) => this.handleChange('lvl', event.target.value)}
                                    />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Submit
                        </Button>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        )
    }
}

export default Player